![CASTEST 图标](./images/castest.jpg)

# ICTest Fault Simulator

## 简介
`ICTest Fault Simulator`是由中科鉴芯（`CASTest`）开源的一款故障仿真器。

中科鉴芯成立于2021年，由中国科学院计算技术研究所全资注入的北京中科算源资产管理有限公司和计算所数字电路测试骨干团队共同投资创立。公司专注于面向数字电路全生命周期质量保障的芯片设计，提供EDA工具授权、IP核和技术服务，涵盖测试、验证、可靠、安全等技术方向。

该开源代码除故障仿真器外，还包含了网表解析器、向量解析器、等效故障生成器、逻辑仿真器等，目前支持全扫描链电路、扫描移入+单次捕获+扫描移出、单时钟域多触发沿、组合逻辑固定型故障。其中，多触发沿指寄存器可能是上升沿触发也可能是下降沿触发，为此，在故障仿真器中采用了三帧仿真技术，将一个时钟周期划分为3个帧进行仿真。网表解析器目前仅支持使用特定工艺库进行综合并插入扫描链后得到的`verilog`网表文件，并解析生成。`ICTtest`系列工具支持的`vy`格式文件，在本开源代码中，我们也提供了一些解析好的基准电路`vy`文件。向量解析器目前仅支持`stil`格式文件。



## 目录

- bench

基准电路的`vy`文件，是使用`smic 180nm`工艺库对`ISCAS‘89`基准电路进行综合并插入扫描链，然后使用`ICTest`网表解析器解析得到的`vy`文件，每个基准电路包括`primitive.vy`和`cell.vy`两个文件。


- conf

配置文件

- script

脚本文件

- src

项目代码文件

- stil

`.stil`格式文件，仿真时需要读取此文件获取测试向量等。

- test

功能测试代码

- thirdparty

第三方开源工具库：
1. 命令行工具：cmdline
   来源：https://github.com/tanakh/cmdline


## 使用说明
* 运行要求

c++11, linux环境

* 编译

~~~
mkdir build
cd build
cmake ..
make
~~~

* 执行指令
~~~
cd build

./ictest 
-e
run_fsim
-c
../bench/testcases/s382_cells.vy
-p
../bench/testcases/s382_primitives.vy
-s
../stil/s382_scan.stil
-o
../out/
~~~
- `-e`表示`exec`，执行指令包括`create_fault`、`run_lsim`、`run_fsim`。其中`run_lsim`表示执行逻辑仿真，`run_fsim`表示执行故障仿真。
- `-c`表示`cell`，指定`cell.vy`的文件路径。
- `-p`表示`primitive`，指定`primitive.vy`的文件路径。
- `-s`表示`stil`，指定`.stil`的文件路径。
- `-o`表示`out`，指定执行结果的输出文件夹路径。

   