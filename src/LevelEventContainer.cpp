///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////


#include "LevelEventContainer.h"

void LevelEventContainer::add(Gate *gptr, int word_id) {
    int level = gptr->GetDpi();
    level_events[level].insert(gptr);
    if (evnet_active_words_map.find(gptr) != evnet_active_words_map.end()) {
        evnet_active_words_map[gptr] = vector<int>(1, word_id);
    } else {
        evnet_active_words_map[gptr].push_back(word_id);
    }
}

// this function return the active event
void LevelEventContainer::next(Gate *&nextEvent, vector<int> &word_ids) {
    nextEvent = getNextEvent();

    if (nextEvent == nullptr && pass != MAX_PASS) {
        pass = pass + 1;
        curr_level = 0;
        nextEvent = getNextEvent();
    }
    if (nextEvent != nullptr) {
        word_ids = evnet_active_words_map[nextEvent];
    } else {
        curr_level = 0;
        pass = 0;
    }
}

Gate *LevelEventContainer::getNextEvent() {
    Gate *next_event = nullptr;
    while (curr_level < total_level) {
        if (level_events[curr_level].size() == 0) {
            curr_level++;
        } else {
            auto &curr_events = level_events[curr_level];
            next_event = *(curr_events.begin());
            int init = next_event->GetId();
            curr_events.erase(curr_events.begin());
            //check earse not change the value
            assert(next_event->GetId() == init);
            break;
        }
    }
    return next_event;
}
