///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#include "Value.h"
void Value::SetSerial1(){
    bit0 = 0x1;
    bit1 = 0x0;
}
void Value::SetSerial0(){
    bit0 = 0x0;
    bit1 = 0x1;
}
void Value::SetSerialX(){
    bit0 = 0x0;
    bit1 = 0x0;
}
void Value::SetSerialZ(){
    bit0 = 0x1;
    bit1 = 0x1;
}
void Value::SetParallel1(int num_bit){
    bit1 = 0x0;

    if(num_bit == SIZE_OF_PACKET){
        bit0 = 0xffffffffffffffff;
    }else{
        bit0 = 1;
        bit0 = (bit0 << num_bit) - 1;
    }
}
void Value::SetParallel0(int num_bit){
    bit0 = 0x0;
    if(num_bit == SIZE_OF_PACKET){

        bit1 = 0xffffffffffffffff;
    }else{
        bit1 = 1;
        bit1 = (bit1 << num_bit) - 1;
    }
}
void Value::SetParallelX(int num_bit){
    bit0 = 0x0;
    bit1 = 0x0;
}
void Value::SetParallelZ(int num_bit){
    if(num_bit == SIZE_OF_PACKET){

        bit0 = 0xffffffffffffffff;
        bit1 = 0xffffffffffffffff;
    }else{
        bit0 = 1;
        bit0 = (bit0 << num_bit) - 1;
        bit1 = 1;
        bit1 = (bit1 << num_bit) - 1;
    }

}
bool Value::operator==(const Value& other_value){
    return (bit0 == other_value.bit0) && (bit1 == other_value.bit1);
}
bool Value::operator!=(const Value& other_value){
    return (bit0 != other_value.bit0) || (bit1 != other_value.bit1);
}

Value& Value::operator=(const Value& rvalue){
    bit0 = rvalue.bit0;
    bit1 = rvalue.bit1;
    return *this;
}

void Value::PrintParallelGateValue(){
    uint64_t new_bit0 = bit0;
    uint64_t new_bit1 = bit1;
    string res;

    if(new_bit0 == 0 && new_bit1 == 0){
        res = "X";
    }
    while(new_bit0 != 0 || new_bit1 != 0){
        int tmp_bit0 = new_bit0 & 1;
        int tmp_bit1 = new_bit1 & 1;

        if( tmp_bit0 == 0  && tmp_bit1 == 0){
            res = "X" + res;
        }else if(tmp_bit0 == 0  && tmp_bit1 == 1){
            res = "0" + res;
        } else if(tmp_bit0 == 1  && tmp_bit1 == 0){
            res ="1" + res;
        }else{
            res = "Z" + res;
        }
        new_bit0 = new_bit0 >> 1;
        new_bit1 = new_bit1 >> 1;

    }
    cout << res<<endl;
}
