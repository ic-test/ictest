///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////


#include "ExecutionEngine.h"
#include "Parameters.h"

int main(int argc, char *argv[]) {

    Params params;
    params.ParseCheck(argc, argv);
    string action = params.GetExecCommand();
    ExecutionEngine atpg;
    // parse netlist
    atpg.ExecNetlistParsing(params);
    if (action == "create_fault") {
        // create fault list
        atpg.ExecFaultListGeneration(params);
    } else if (action == "run_lsim") {
        // run logic simulation
        atpg.ExecStilReading(params);
        atpg.ExecLogicSimulation(params);
    } else if (action == "run_fsim") {
        // run fault simulation
        atpg.ExecFaultListGeneration(params);
        atpg.ExecStilReading(params);
        atpg.ExecFaultSimulation(params);
    } else {
        string msg = "not support execution command: " + action;
        cerr << Errors::ErrorsMsg(ErrorType::INVALID, msg) << endl;
        exit(1);
    }
    return 0;
}

