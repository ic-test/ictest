///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#include "Utils.h"

bool Utils::CheckFileExist(const std::string& file_name) {
    std::ifstream ifs(file_name);
    return ifs.is_open();
}

double Utils::GetSysTimeInSeconds(std::chrono::system_clock::time_point& start_t,
                                  std::chrono::system_clock::time_point& end_t ) {
    std::chrono::duration<double> time_span =
            std::chrono::duration_cast<std::chrono::duration<double>>(end_t - start_t);
    return time_span.count();
}

double Utils::GetCpuTimeInSeconds(std::clock_t & start_cpu_t,
                                  std::clock_t & end_cpu_t) {
    double cpu_duration = (end_cpu_t - start_cpu_t) / (double)CLOCKS_PER_SEC;
    return cpu_duration;
}

// split string
std::vector<std::string> Utils::split(std::string str, std::string delim) {
    std::regex re{ delim };
    return std::vector<std::string> {
            std::sregex_token_iterator( str.begin(), str.end(), re, -1 ),
            std::sregex_token_iterator()
    };
//    std::vector<std::string> tokens;
//    auto start = 0U;
//    auto end = str.find(delim);
//    while (end != std::string::npos) {
//        tokens.push_back(str.substr(start, end - start));
//        start = end + delim.length();
//        end   = str.find(delim, start);
//    }
//    return tokens;
}
