///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////


#ifndef DEFINE_H
#define DEFINE_H
#include "Setup.h"

//#define DEBUG
// function return status
#define RET_OK  0
#define RET_ERR -1
///////////////////////////////
// define supported gate type
#define  G_PI     1
#define  G_PO     2
#define  G_PPI    3
#define  G_PPO    4
#define  G_AND    6
#define  G_NAND   7
#define  G_OR     8
#define  G_NOR    9
#define  G_NOT    10
#define  G_BUF    11
// a branch of stem
#define  G_STEM   12
// bi-direction
#define  G_BIDIR  13
#define  G_BUS    14
#define  G_XOR    101
#define  G_XNOR   102
#define  G_BUF_ASSGIN    180
#define  G_SUPPLYX 181
#define  G_SUPPLYZ 182
#define  G_WIRE    191
#define  G_TSHI    192
#define  G_TSH     193
#define  G_PULL    194
#define  G_SUPPLY0 195
#define  G_SUPPLY1 196
#define  G_MUX     197
#define  G_DFF     198
#define  G_DLAT    199
// G_BASIC is a flag
#define  G_BASIC   199


///////////////////////////////
// logic system of ATPG.
#define LOGIC_ZERO  0
#define LOGIC_ONE   1
#define LOGIC_X     2
#define LOGIC_D     3
#define LOGIC_DB    4

//////////////////////////////////
// some special gate type, used to identify these gate for help
enum SpecialGate {
    NONE= 0, CLK_GATE, DFF_GATE, SET_GATE, RESET_GATE, SUPPLY_GATE,
    TEST_SE_GATE, TEST_SI_GATE, TEST_SO_GATE
};

//////////////////////////////////
// define fault type : stuck at 0, stuck at 1
enum FaultType {
    SA0 = 0,
    SA1 = 1
};

//////////////////////////////////
// define fault status
// ABORT, DETECTED, UNDETECTED, REDUNDANT
enum FaultStatus {
    DETECTED = 0,   // the fault is detected
    UNDETECTED = 1, // the initial status of all faults
    ABORT = 2,      // exceed the backtrack limits and the fault is not detected
    REDUNDANT = 3   // decision tree is empty and the fault is not detected
};
const string FAULT_STATUS[] = { "DETECTED", "UNDETECTED", "ABORT", "REDUNDANT" };
//////////////////////////////////
// return atpg run status
enum ATPGStatus {
    TRUE = 0,   // atpg run success, the fault is detected
    FALSE = 1,  // atpg run failure, the fault is redundant
    MAYBE = 2   // fault status is abort, the fault maybe detected
};

enum lineType {
    LFREE = 100,
    HEAD,
    BOUND
};

enum SRstatus {
    DEFAULT,
    ENCOUNTERED,
    PRIM_RECONV_GATE,
    SEC_RECONV_GATE,
    REMOVED,
    REREMOVED
};

//////////////////////////////////
#define SIZE_OF_PACKET 64
#define MAX_THREAD_NUM 64
#define MAX_FUTURE_NUM 1000000

#endif // DEFINE_H