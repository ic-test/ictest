///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef LEVEL_EVENT_CONTAINER_H
#define LEVEL_EVENT_CONTAINER_H

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include "Gate.h"
#include "EventContainer.h"
#define MAX_PASS 2
class LevelEventContainer : public EventContainer {
private:
    // store all active events
    std::vector<std::unordered_set<Gate *>> level_events;
    std::unordered_map<Gate *, std::vector<int>> evnet_active_words_map;
    int curr_level = 0;
    int total_level = 0;
    // every frame should two pass through all levels to get all active events
    // because DFF may have feedback output path produce  active event,
    // the second pass deal with these events
    int pass = 1;
    // return value = -1 represent this pass no active event
    // return value >= 0 represent the minimum level that contain active event in this pass
    Gate *getNextEvent();

public:
    LevelEventContainer(int totalLevel) {
        level_events = vector<std::unordered_set<Gate *>>(totalLevel);
        curr_level = 0;
        total_level = totalLevel;
        pass = 1;
    }
    ~LevelEventContainer() {
    }

    void next(Gate *&nextEvent, vector<int> &word_ids) override;
    void add(Gate *, int word_id) override;
    void clear() override {
        for (int i = 0; i <level_events.size() ; ++i) {
            level_events[i].clear();
        }
        curr_level = 0;
        pass = 0;
    }
};
#endif // LEVEL_EVENT_CONTAINER_H