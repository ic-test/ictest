///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef EXECUTION_ENGINE_H
#define EXECUTION_ENGINE_H

#include "Simulation.h"
#include <chrono>

class ExecutionEngine : public Simulation {
private:
    void ExecMultiThreadLogicSim();
    void ExecMultiThreadFaultSim(vector<Fault*>& flist);
public:
    ExecutionEngine()  {};
    ~ExecutionEngine() {};

    void ExecNetlistParsing(Params& params);
    void ExecFaultListGeneration(Params& params);
    void ExecStilReading(Params& params);
    void ExecLogicSimulation(Params& params);
    void ExecFaultSimulation(Params& params);
protected:

};
#endif // EXECUTION_ENGINE_H