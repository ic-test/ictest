///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef ERRORS_H
#define ERRORS_H

#include <string>
using std::string;

enum class ErrorType {
    // common error [0-9]
    INVALID             = 0,
    UNKNOWN_TYPE        = 1,
    FILE_NOT_FOUND      = 2,
    // parse netlist error [10-19]
    NETLIST_PARSE_FAILD         = 10,
    NETLIST_PREPROCESS_FAILED   = 11,
    // create fault list error [20-29]
    FLIST_CREATE_FAILED         = 20,
    // run atpg error [30-39]
    ATPG_RUN_FAILED     = 30,
    // run simulation error [40-49]
    LSIM_RUN_FAILED     = 40,
    FSIM_RUN_FAILED     = 41,
    // parse params error [50-59]
    PARAM_PARSE_FAILED  = 50,

};

struct Errors {

    static string ErrorsMsg(ErrorType etype, const string &msg) {
        string error_msg = "Error Type:: " + ErrorTypeToString(etype) + " | MSG:: " + msg;
        return error_msg;
    }

    static string ErrorTypeToString(ErrorType etype) {
        switch (etype) {
            case ErrorType::INVALID :
                return "Invalid";
            case ErrorType::UNKNOWN_TYPE :
                return "Unknown Type";
            case ErrorType::FILE_NOT_FOUND :
                return "File Not Found";
            case ErrorType::NETLIST_PARSE_FAILD :
                return "Netlist Parse Failed";
            case ErrorType::NETLIST_PREPROCESS_FAILED :
                return "Netlist Preprocess Failed";
            case ErrorType::FLIST_CREATE_FAILED :
                return "Flist Create Failed";
            case ErrorType::ATPG_RUN_FAILED :
                return "ATPG Run Failed";
            case ErrorType::LSIM_RUN_FAILED :
                return "LSIM Run Failed";
            case ErrorType::FSIM_RUN_FAILED :
                return "FSIM Run Failed";
            case ErrorType::PARAM_PARSE_FAILED :
                return "Params Parse Failed";
            default :
                return "Unknown";
        }
    }
};

#endif // ERRORS_H
