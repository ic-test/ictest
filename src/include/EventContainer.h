///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////


#ifndef EVENT_CONTAINER_H
#define EVENT_CONTAINER_H
#include "Gate.h"
class EventContainer {
public:
    virtual void next(Gate *&nextEvent, vector<int> &word_ids) = 0;
    virtual void add(Gate *gptr, int word_id) = 0;
    virtual void clear() = 0;

    ~EventContainer(){};
};
#endif // EVENT_CONTAINER_H