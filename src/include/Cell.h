///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef CELL_H
#define CELL_H

#include "Define.h"

class Cell {

public:

    Cell() {};
    Cell(int id, int logic_type, string name)
        : _id(id), _logic_type(logic_type), _inst_name(name) {}

    ~Cell() {}
    // basic set/get
    void SetId(int id) { _id = id;   }
    int  GetId() const { return _id; }

    void SetLogicType(int logic_type) { _logic_type = logic_type; }
    int  GetLogicType() const         { return _logic_type; }

    void   SetInstName(string name) { _inst_name = name; }
    string GetInstName() const      { return _inst_name; }

    void AddInputs(int pin_id, int net_id) { _inputs.insert( { pin_id, net_id } ); }
    unordered_map<int, int>& GetInputs()   { return _inputs; }

    void AddInouts(int pin_id, int net_id) { _inouts.insert( { pin_id, net_id } ); }
    unordered_map<int, int>& GetInouts()   { return _inouts; }

    void AddOutputs(int pin_id, int net_id) { _outputs.insert( { pin_id, net_id } ); }
    unordered_map<int, int>& GetOutputs()   { return _outputs; }

private:
    int     _id;                         // cell unique id
    int     _logic_type;                 // cell logic type id
    string  _inst_name;                  // cell instance name
    unordered_map<int, int> _outputs;    // cell outputs pin & net id
    unordered_map<int, int> _inouts;     // cell inouts pin & net id
    unordered_map<int, int> _inputs;     // cell inputs pin & net id

};

#endif // CELL_H