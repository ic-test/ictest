///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef VALUE_H
#define VALUE_H
#include <cstdint>
#include <iostream>
#include "Define.h"
using namespace std;

// bit0 = 0 bit1 = 1 represent 0
// bit0 = 1 bit1 = 0 represent 1
// bit0 = 0 bit1 = 0 represent X
// bit0 = 1 bit1 = 1 represent Z
class Value {
public:
    uint64_t bit0;
    uint64_t bit1;
    void SetSerial1();
    void SetSerial0();
    void SetSerialX();
    void SetSerialZ();
    void SetParallel1(int num_bit);
    void SetParallel0(int num_bit);
    void SetParallelX(int num_bit);
    void SetParallelZ(int num_bit);
    void PrintParallelGateValue();
    bool operator==(const Value& other_value);
    bool operator!=(const Value& other_value);
    Value& operator=(const Value& rvalue);
    Value():bit0(0x0),bit1(0x0){}
    Value(uint64_t  bit0_ , uint64_t  bit1_):bit0(bit0_), bit1(bit1_){}
};


#endif //VALUE_H
