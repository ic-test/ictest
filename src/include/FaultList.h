///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef FAULTLIST_H
#define FAULTLIST_H

#include "Netlist.h"
#include "Errors.h"

class FaultList : public Netlist {
public:
    FaultList()  {};
    ~FaultList() {};

    void CreateCellFaultList();
    // Generate collapsed fault lists
    void CreateCellEqvFaultList();
    // Check fault list equivalent realtion
    void CheckEqvRelation(Gate* gptr, int sa);

    // Get fault list
    vector<Fault*>& GetUncollapsedFlist() { return _uflist; }
    vector<Fault*>& GetCollapsedFlist()   { return _cflist; }
    // Print fault list information
    void PrintFaultList(ostream& out, vector<Fault*>& flist);

protected:
    // uncollapsed full fault list
    vector<Fault*> _uflist;
    // collapsed fault list
    vector<Fault*> _cflist;
    // for eqv fault check
    unordered_map<int, int> _group_id;
    vector<list<int>>       _eqv_group;
};
#endif // FAULTLIST_H
