///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <chrono>
#include <algorithm>
#include <regex>
#include <fstream>

class Utils{
public:
    static bool   CheckFileExist(const std::string& file_name);
    static double GetSysTimeInSeconds(std::chrono::system_clock::time_point& start_t,
                                      std::chrono::system_clock::time_point& end_t);
    static double GetCpuTimeInSeconds(std::clock_t& start_cpu_t,
                                      std::clock_t& end_cpu_t);
    static std::vector<std::string> split(std::string str,
                                          std::string delim);
};

#endif // UTILS_H