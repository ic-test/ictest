///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef SETUP_H
#define SETUP_H
/////////////////////////////////////////
//os: choose LINUX or WINDOWS environment
#define LINUX
//#define WINDOWS

/////////////////////////////////////////
// all includes 
#ifdef LINUX
#include <sys/time.h>
#include <string.h>
#endif

#ifdef WINDOWS
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <string>
#endif

#include <random>
#include <future>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <vector>
#include <map>
#include <set>
#include <thread>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cassert>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <sstream>
#include <list>
#include <algorithm>
#include <cstdint>
#include <functional>
#include <float.h>
#include <chrono>
#include <climits>
using namespace std;

#endif // SETUP_H