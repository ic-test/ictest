///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef ATPG_STILPARSER_H
#define ATPG_STILPARSER_H

#include "Setup.h"

class STILparser{

//-------------------------------------------------------------------------------------------------------------
//the STIL_parser.
private:
    void STIL_parser_init();

    bool is_digit(char ch);
    bool is_letter(char ch);
    void time_expr(vector<string> &type, vector<string> &tokens);
    void scan(vector<string> &type, vector<string> &tokens, string filename);

    void err_exit();
    void pass(const string &t);
    bool int_t();
    bool id();
    bool is_pi();
    bool is_po();

    bool float_t();
    bool wfc_seq();
    bool time_expr();
    bool event_code();

    //////////////////////////////////////////////////////////////////
    //ASSIGS.
    bool repeat();
    void assig_expr();
    bool assig();
    void assigs();

    //INST LIST.
    bool iddq_inst();
    bool stop_inst();
    bool macro_inst();
    bool call_inst();
    bool v_inst();
    bool f_inst();
    bool c_inst();
    bool w_inst();
    bool shift();
    bool loop();
    bool label();
    bool inst();
    void inst_list();

    //PATTERNS.
    bool pattern();
    void pattern_l();

    //MACRO.
    void macros_l();
    bool macros();
    bool macro();

    //PROCEDURE_L.
    void procedures_l();
    bool procedures();
    bool procedure();

    //PATTERN_EXEC.
    bool pattern_exec_l();
    bool pattern_exec();
    bool pattern_burst_call();

    //PATTERN_BURST_L.
    bool pattern_burst_l();
    bool pattern_burst();
    void context();
    bool macro_context();
    bool proced_context();
    bool pattern_list();
    bool pattern_call();

    //SCAN STRUCTURES.
    void scan_clock();
    void scan_cells();
    void scan_inversion();
    void scan_out();
    void scan_in();
    void scan_length();
    bool scan_chain();
    bool scan_structures();
    void scan_structures_l();

    //TIMING.
    bool timing();
    bool waveform_table();
    bool period();
    void waveforms();
    bool waveform();
    bool event();

    //SIGNAL GROUP.
    bool signal_groups();
    bool signal_group();
    bool signal_list(vector<string>* singal_names);

    //SIGNALS.
    bool signals();
    bool signal();
    bool signal_dir();
    void signal_attributes();
    bool signal_scan();
    void wfc_map();
    bool map_rule();

    //HEADER.
    void header();
    void title();
    void date();
    void source();
    void history();

    //FORMAT.
    bool format();
    bool design();

    //PROGRAM.
    void program();

    void process_pattern_result(string path_operation);
    void process_scan_structure(string path_scancell_file);

    vector<string> type;
    vector<string> tokens;
    int len;
    int i;

    vector<string> pattern_test;
    vector<string> procs;
    vector<string> scan_structure;
    vector<string> pi_singal;
    vector<string> po_singal;
    vector<string> si_singal;
    vector<string> so_singal;
    vector<string> clk_singal;
    vector<pair<string,int>> clock_waveform;
    string proc;

public:
    void print(vector<string> &type, vector<string> &tokens);
    void parse(string path_external_stil);

    STILparser(){
        STIL_parser_init();
    }
    vector<string> GetStilPi(){
        return pi_singal;
    }
    vector<string> GetStilPo(){
        return po_singal;
    }
    vector<string> GetStilSi(){
        return si_singal;
    }
    vector<string> GetStilSo(){
        return so_singal;
    }
    vector<pair<string,int>> GetStilClk(){
        auto begin_it = clock_waveform.begin();
        auto end_it = clock_waveform.begin()+ clk_singal.size();
        vector<pair<string,int>> rst(begin_it,end_it);
        return rst;
    }
};

#endif //ATPG_STILPARSER_H
