///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef PARAMETERS_H
#define PARAMETERS_H
#include "cmdline.h"
#include "Setup.h"
#include "Utils.h"

// atpg params define
class Params {

public:
    Params() {
        _option.add<string>( "exec", 'e', "execute command: create_faults, run_atpg, run_sim, run_fsim", true, "" );
        _option.add<string>( "primitive", 'p', "read primitive vy file", true, "" );
        _option.add<string>( "cell", 'c', "read cell vy file", true, "" );
        _option.add<string>( "output", 'o', "output directory path.", false, "" );
        _option.add<string>( "stil", 's', "read external STIL file", false, "" );
    }

    void ParseCheck(int argc, char* argv[]) {
        _option.parse_check( argc, argv );
        _exec_command       = _option.get<string>( "exec" );
        _primitive_vy_file  = _option.get<string>( "primitive" );
        _cell_vy_file       = _option.get<string>( "cell" );
        _output_dir         = _option.get<string>( "output" );
        _external_stil_file = _option.get<string>("stil");

        vector<string> vec = Utils::split( _primitive_vy_file, "/|\\\\" );
        string circuit_name = vec[vec.size() - 1];
        circuit_name = circuit_name.substr( 0, circuit_name.rfind( ".vy" ) );

        _pattern_file  = _output_dir + "/" + circuit_name + ".pat";
        _fault_file    = _output_dir + "/" + circuit_name + ".flist";
        _atpg_report   = _output_dir + "/" + circuit_name + ".atpg_rpt";
        _fsim_report   = _output_dir + "/" + circuit_name + ".fsim_rpt";
        _parser_report = _output_dir + "/" + circuit_name + ".log";
        _lsim_report   = _output_dir + "/" + circuit_name + ".sim_rpt";
    }

    string GetPrimitiveVyFile()   { return _primitive_vy_file;  }
    string GetCellVyFile()        { return _cell_vy_file;   }
    string GetExecCommand()       { return _exec_command;   }
    string GetExternalStilFile()  { return _external_stil_file; }
    string GetPatternFile()       { return _pattern_file;   }
    string GetFaultFile()         { return _fault_file;     }
    string GetAtpgReport()        { return _atpg_report;    }
    string GetFsimReport()        { return _fsim_report;    }
    string GetParserReport()      { return _parser_report;  }
    string GetLsimReport()        { return _lsim_report;    }

private:
    cmdline::parser _option;
    string _primitive_vy_file;
    string _cell_vy_file;
    string _exec_command;
    string _external_stil_file;
    string _pattern_file;
    string _fault_file;
    string _lsim_report;
    string _atpg_report;
    string _fsim_report;
    string _parser_report;
    string _output_dir;

};

#endif // PARAMETERS_H