///////////////////////////////////////////////////////////////////////////////
//    Copyright (c) 2021 CASTest Corporation Limited. All Rights Reserved    //
///////////////////////////////////////////////////////////////////////////////

#ifndef FAULT_H
#define FAULT_H

#include "Gate.h"
class Fault {

public:
    Fault() {};
    Fault(int id, Gate* fault_gate,
          FaultStatus fault_status, FaultType fault_type,
          string fault_name = "",   string fault_eqv = "")
          : _id( id ),
            _fault_gate( fault_gate ),
            _fault_status( fault_status ),
            _fault_type( fault_type ),
            _fault_name( fault_name ),
            _fault_eqv( fault_eqv ) {}

    ~Fault() {}

    // basic set/get
    void SetFaultId(int id) { _id = id;     }
    int  GetFaultId() const { return _id;   }

    void   SetFaultName(string fault_name) { _fault_name = fault_name; }
    string GetFaultName() const { return _fault_name;       }

    void  SetFaultGate(Gate* fault_gate) { _fault_gate = fault_gate; }
    Gate* GetFaultGate() const { return _fault_gate;       }

    void SetFaultStatus(FaultStatus fault_status) { _fault_status = fault_status; }
    FaultStatus GetFaultStatus() const { return _fault_status;         }

    void SetFaultType(FaultType fault_type) { _fault_type = fault_type; }
    FaultType GetFaultType() const { return _fault_type;       }
    
    void   SetFaultEqv(string eqv) { _fault_eqv = eqv;  }
    string GetFaultEqv() const { return _fault_eqv; }

    // print fault information
    friend ostream& operator<<(ostream& os, Fault& fptr);

private:
    int _id;                           // fault id
    Gate* _fault_gate;                 // fault gate
    FaultStatus _fault_status;         // fault status: detected, undetected...
    FaultType _fault_type;             // fault type: sa1, sa0
    string _fault_name;                // fault name, exp: "==inst0==A"
    string _fault_eqv;                 // equivalent fault represent: "NC" or "--"
};

inline ostream& operator<<(ostream& os, Fault& fptr) {
    os << setw(4)  << fptr._fault_gate->GetId()   << " ";
    os << setw(25) << fptr._fault_gate->GetName() << " ";
    string sa = fptr._fault_type == SA0 ? "SA0" : "SA1";
    os << setw(4) << sa << " " << setw(4) << fptr._fault_eqv << " " << setw(10) << fptr._fault_name << " ";
    os << setw(10) << FAULT_STATUS[fptr._fault_status] ;
    return os;
}

#endif // FAULT_H